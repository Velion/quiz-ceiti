var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var session = require('express-session');
var expressValidator = require('express-validator');
var passport = require('passport');
var flash = require('connect-flash');
var LocalStrategy = require('passport-local').Strategy;
var cookieSession = require('cookie-session');

var connection = require('./configs/connection');
var index = require('./routes/index.route');
var quiz = require('./routes/quiz.route');
var admin = require('./routes/admin.route');
var quizList = require('./routes/quiz-list.route');
var fetchData = require('./routes/fetch-data.route');

var app = express(),
	handlebars;

require('./configs/passport')(passport);//for log in

handlebars = exphbs.create({
	extname: '.html',
	helpers: {
		toJSON: function (object) {
			return (JSON.stringify(object))
		},
		consoleLog: function (object) {
			console.log(object);
		},
		braces: function (object) {
			return '{{' + object + '}}';
		},
		ifCond: function (v1, operator, v2, options) {
			switch (operator) {
				case '==':
					return (v1 == v2) ? options.fn(this) : options.inverse(this);
				case '===':
					return (v1 === v2) ? options.fn(this) : options.inverse(this);
				case '!=':
					return (v1 != v2) ? options.fn(this) : options.inverse(this);
				case '!==':
					return (v1 !== v2) ? options.fn(this) : options.inverse(this);
				case '<':
					return (v1 < v2) ? options.fn(this) : options.inverse(this);
				case '<=':
					return (v1 <= v2) ? options.fn(this) : options.inverse(this);
				case '>':
					return (v1 > v2) ? options.fn(this) : options.inverse(this);
				case '>=':
					return (v1 >= v2) ? options.fn(this) : options.inverse(this);
				case '&&':
					return (v1 && v2) ? options.fn(this) : options.inverse(this);
				case '||':
					return (v1 || v2) ? options.fn(this) : options.inverse(this);
				default:
					return options.inverse(this);
			};
		},
		counter: function (index) {
			return index + 1;
		},
		math: function (lvalue, operator, rvalue, options) {
			lvalue = parseFloat(lvalue);
			rvalue = parseFloat(rvalue);

			return {
				"+": lvalue + rvalue,
				"-": lvalue - rvalue,
				"*": lvalue * rvalue,
				"/": lvalue / rvalue,
				"%": lvalue % rvalue
			}[operator];
		}
	}
});

app.engine('html', handlebars.engine);

app.set('port', 3000);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

// express session and passport initialization
// app.use(session({
// 	secret: 'secret',
// 	saveUninitialized: true,
// 	resave: true
// }));
var sessionAdmin = session({
	secret: 'secret',
	saveUninitialized: true,
	resave: true
});
var passportInit = passport.initialize();
var sessionInit = passport.session();
var toSendMiddlewareAdmin = [sessionAdmin, passportInit, sessionInit];

var cookiesUser = cookieSession({
	name: 'sessionUser',
	keys: ['try']
});

// passport init
// app.use(passport.initialize());
// app.use(passport.session());
// app.use(flash()); //trebuie de pus in template engine ca sa se vada

app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/quiz', cookiesUser, quiz);
app.use('/admin', toSendMiddlewareAdmin, admin);
app.use('/admin', authorization, admin);
app.use('/quiz-list', quizList);
app.use('/data', fetchData);

// start logging
// render login page
app.get('/sign-in', function (req, res) {
	res.render('./admin/sign-in');
});

// handle sign-in request 
// app.post('/sign-form', passport.authenticate('local-login', {
// 	successRedirect: '/admin/dashboard', // redirect to the secure profile section
// 	failureRedirect: '/sign-in' // redirect back to the signup page if there is an error
// }), function (req, res) {
// 	console.log("hello");
// 	if (req.body.remember) {
// 		req.session.cookie.maxAge = 900000; //15 minutes
// 	} else {
// 		req.session.cookie.expires = false;
// 	};
// 	res.redirect('/admin/sign-in');
// });

//logout
//go to dashboard
// router.get('/profile', authorization, function (req, res) {
// 	res.render('./admin/dashboard',{
// 		user : req.user // get the user out of session and pass to template

// 	});
// });

function authorization(req, res, next) {
	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();
	// user: req.user ucide sesiunea userului
	// if they aren't redirect them to the home page
	res.redirect('/admin/sign-in');
}
// function authorizationUser(req, res, next) {
// 	// if user is authenticated in the session, carry on
// 	var cookie = req.cookies.cookieName;
// 	if (cookie === undefined) {
// 		console.log("fails");
// 		res.redirect('/');
// 	}
// 	else {
// 		// yes, cookie was already present 
// 		console.log('cookie exists', cookie);
// 		return next();
// 	}
// }
// function authorizationUser(req, res, next) {
// 	// if user is authenticated in the session, carry on
// 	if (req.isAuthenticated())
// 		return next();
// 	// user: req.user ucide sesiunea userului
// 	// if they aren't redirect them to the home page
// 	res.redirect('/');
// }

// app.use(session({
//     secret: 'W2IFU-I5-L2i4U',
//     resave: true,
//     saveUninitialized: true
// }));

// // authentication and authorization Middleware
// var auth = function(req, res, next) {
//   if (req.session && req.session.user === 'vixan' && req.session.admin)
//     return next();
//   else
//     return res.sendStatus(401);
// };

// // login endpoint
// app.get('/login', function (req, res) {
//   if (!req.query.username || !req.query.password) {
//     res.send('login failed');    
//   } else if(req.query.username === 'vixan' || req.query.password === 'master') {
//     req.session.user = 'vixan';
//     req.session.admin = true;
//     res.send('login success!');
//   }
// });

// // get admin page endpoint
// app.use('/admin', auth, admin);

// // logout endpoint
// app.get('/logout', function (req, res) {
//   req.session.destroy();
//   res.send('logout success!');
// });

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500).render('error', { status: res.statusCode });
});

app.listen(app.get('port'), function () {
	console.log('Quiz App listening on port ' + app.get('port'));
});

module.exports = app;