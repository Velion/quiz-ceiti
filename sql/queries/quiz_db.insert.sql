use quiz_db;
-- data
INSERT INTO topics (topic_name, knowledge_of)
VALUES
  (
    'Comunitatea de rețele Internet',
    'Retele de calculatoare'
  ),
  (
    'Structura documentului HTML. Formatarea caracterelor',
    'HTML'
  ),
  (
    'Hypertext. Noțiuni și concepte de bazã.',
    'HTML'
  ),
  (
    'Liste. Noțiuni și Marcaje utilizate.',
    'HTML'
  ),
  (
    'Tabele. Noțiuni și Marcaje utilizate.',
    'HTML'
  ),
  (
    'Operarea cu obiecte. Inserarea imaginilor, sunetelor și videoclipurilor.',
    'HTML'
  ),
  (
    'Referințe. Referințe interne și externe.',
    'HTML'
  ),
  (
    'Operarea cu formulare. Marcaje utilizate.',
    'HTML'
  ),
  (
    'Ferestre în HTML. Comenzi de construire a ferestrelor.',
    'HTML'
  ),
  ('Inițiere în CSS.', 'CSS'),
  ('Stiluri CSS.', 'CSS'),
  ('Interfața aplicației.', 'CSS'),
  ('Aplicația Dreamweaver.', 'CSS'),
  (
    'Limbajul PHP - inițiere.',
    'PHP'
  ),
  ('Limbajul PHP - mediu.', 'PHP'),
  ('Limbajul PHP - avansat.', 'PHP'),
  (
    'Limbajul Javascript - inițiere.',
    'Javascript'
  );

INSERT INTO answers (answer_name)
VALUES
	('anii 40 ai sec.XX'), 
	('secolul XIX.'), 
	('nu se ştie cind a apărut'), 
	('anii 60 ai sec.XX'),
	('un calculator ce oferă o legatură tranzitivă în reţea'),
	('firma specializată ce oferă sevicii de conectare la Internet'),
	('o persoană care asigură conectarea la Internet'),
	('program client de conectare la reţea'),
	('Caracteristicile tehnice ale calculatorului'),
	('Calitatea liniei de conectare'),
	('Tipul conectării'),
	('Toate opţiunile de mai sus'),
	('compania Microsoft'),
	('are un guvern propriu'),
	('nu are un organ de conducere'),
	('compania IBM'),
	('multimedia'),
	('hypertext'),
	('hyperlegatură'),
	('site web'),
	('un calculator care prestează servicii Internet'),
	('un calculator prin care utilizatorii au acces la Internet'),
	('un nod de comunicare'),
	('un canal de transfer de date'),
	('adresa fizică obişnuită: ţara, localitatea, strada, blocul'),
	('adresa unică numita IP adresă'),
	('prin denumirea unui site'),
	('succesiune numerică de 4 bytes în format numeric'),
	('nu este posibil de localizat un calculator'),
	('linie telefonică'),
  ('fibră optică'),
  ('satelit'),
  ('cablu coaxial'),
  ('un dispozitiv ce realizează legătura între 2 reţele'),
  ('un set de reguli de transmitere a datelor între 2 calculatoare'),
  ('un contract de conectare la Internet'),
  ('un limbaj comun de transmitere a datelor între 2 calculatoare'),
  ('un limbaj de programare'),
  ('nu cunosc aşa termen'),
  ('reţea locală de calculatoare'),
  ('reţea globală de calculatoare'),
  ('reţea de reţele de calculatoare'),
  ('protocolul oficial al Internet-ului'),
  ('un dispozitiv de transmitere a datelor'),
  ('limbaj de programare în Internet'),
  ('nu cunosc aşa noţiune'),
  ('un limbaj de programare'),
  ('o reţea ARPA'),
  ('o reţea zonală din SUA'),
  ('o reţea DARPA'),
  ('o reţea locală in SUA'),
  ('o reţea de calculatoare'),
  ('hard discul'),
  ('discheta'),
  ('discul optic'),
  ('un calculator prin care utilizatorii au acces la Internet'),
  ('un canal de transmitere de date'),
  ('un nod de comunicare'),
  ('un calculator care prestează servicii Internet'),
  ('monitorul'),
  ('scanerul'),
  ('modemul'),
  ('mouse-ul'),
  ('microunde'),
  ('linia telefonică'),
  ('analitică'),
  ('de pachete'),
  ('calculatoare client şi server'),
  ('imprimante'),
  ('noduri de comutatie'),
  ('scanere'),
  ('canale de transfer de date'),
  ('un calculator minim cu 166 Mhz, 32 RAM'),
  ('un calculator performant de ultimă generaţie'),
  ('orice calculator'),
	('test_answer1'),
	('test_answer2'),
	('test_answer3'),
	('test_answer4'),
	('test_answer5'),
	('test_answer6'),
	('test_answer7'),
	('test_answer8'),
	('test_answer9'),
	('test_answer10'),
	('test_answer11'),
	('test_answer12'),
	('test_answer13'),
	('test_answer14'),
	('test_answer15'),
	('test_answer16'),
	('test_answer17'),
	('test_answer18'),
	('test_answer19'),
	('test_answer20'),
	('test_answer21'),
	('test_answer22'),
	('test_answer23'),
	('test_answer24'),
	('test_answer25'),
  ('test_answer1'),
  ('test_answer2'),
  ('test_answer3'),
  ('test_answer4'),
  ('test_answer5'),
  ('test_answer6'),
  ('test_answer7'),
  ('test_answer8'),
  ('test_answer9'),
  ('test_answer10'),
  ('test_answer11'),
  ('test_answer12'),
  ('test_answer13'),
  ('test_answer14'),
  ('test_answer15'),
  ('test_answer16'),
  ('test_answer17'),
  ('test_answer18'),
  ('test_answer19'),
  ('test_answer20'),
  ('test_answer21'),
  ('test_answer22'),
  ('test_answer23'),
  ('test_answer24'),
  ('test_answer25');

INSERT INTO question_types (question_type_name)
VALUES
	('radio'),
	('checkbox'),
	('write'),
	('truefalse'),
	('dropdown');

INSERT INTO questions (
	question_name,
	question_type_id,
	points
)
VALUES
	('Internetul a apărut în:', 1, 2),
	('Un ISP (Internet Service Provider) este:', 1, 1),
	('Calitatea serviciilor oferite de o reţea de calculatoare depinde de:', 1, 2),
	('Cine guvernează reţeaua Internet?', 1, 1),
	('Modul de organizare a informatiei pe Web se numeşte:', 1, 2),
	('Un server Web este:', 1, 2),
	('Localizarea unui calculator în Internet se face prin:', 2, 2),
	('Cea mai eficientă metodă de a conecta reţelele dintre 2 continente este:', 1, 2),
	('Protocolul este:', 2, 2),
	('Ce este Internet-ul?', 2, 1),
	('TCP/IP este:', 1, 2),
	('Predecesorul Internetului a fost:', 2, 3),
	('Pentru transmiterea a 7 Mb de informaţie de la un calculator la altul este mai bine de utilizat:', 1, 2),
	('Un terminal (calculator client) este:', 1, 2),
	('Dispozitivul de conectare a calculatoarelor la Internet este:', 1, 2),
	('Cea mai eficientă legatura între calculatoarele unei reţele de calculatoare locale se realizează prin:', 1, 2),
	('Fişierele într-o reţea de calculatoare se transmit în formă:', 1,21),
	('Care din lista de mai jos sunt componentele de bază ale Internetului?', 2, 3),
	('Calculatorul de pe care dorim să ne conectăm la Internet poate fi:', 1, 2),
	('Item1', 1, 2),
	('Item2', 2, 1),
	('Item3', 2, 2),
	('Item4', 3, 3),
	('Item5', 4, 2),
	('Item6', 4, 2),
	('Item7', 3, 3),
	('Item8', 3, 2),
	('Item9', 3, 2),
	('Item10', 3, 1),
	('Item11', 2, 1),
	('Item12', 2, 2),
	('Item13', 2, 2),
	('Item14', 1, 2),
	('Item15', 1, 2),
	('Item16', 5, 1),
	('Item17', 5, 2),
	('Item18', 5, 2);

INSERT INTO question_answers (
  question_id,
  answer_id,
  is_correct
)
VALUES
  (1, 1, 0), (1, 2, 0), (1, 3, 0), (1, 4, 1),
  (2, 5, 0), (2, 6, 1), (2, 7, 0), (2, 8, 0),
  (3, 9, 0), (3, 10, 0), (3, 11, 0), (3, 12, 1),
  (4, 13, 0), (4, 14, 0), (4, 15, 1), (4, 16, 0),
  (5, 17, 0), (5, 18, 1), (5, 19, 0), (5, 20, 0),
  (6, 21, 1), (6, 22, 0), (6, 23, 0), (6, 24, 0),
  (7, 25, 0), (7, 26, 1), (7, 27, 0), (7, 28, 1), (7, 29, 0),
  (8, 30, 0), (8, 31, 0), (8, 32, 1), (8, 33, 0),
  (9, 34, 0), (9, 35, 1), (9, 36, 0), (9, 37, 1), (9, 38, 0),
  (10, 39, 0), (10, 40, 0), (10, 41, 1), (10, 42, 1),
  (11, 43, 1), (11, 44, 0), (11, 45, 0), (11, 46, 0), (11, 47, 0),
  (12, 48, 1), (12, 49, 0), (12, 50, 1), (12, 51, 0),
  (13, 52, 1), (13, 53, 0), (13, 54, 0), (13, 55, 0),
  (14, 56, 1), (14, 57, 0), (14, 58, 0), (14, 59, 0),
  (15, 60, 0), (15, 61, 0), (15, 62, 1), (15, 63, 0),
  (16, 64, 0), (16, 32, 0), (16, 65, 0), (16, 33, 1),
  (17, 66, 0), (17, 67, 1),
  (18, 68, 1), (18, 69, 0), (18, 70, 1), (18, 71, 0), (18, 72, 1),
  (19, 73, 1), (19, 74, 0), (19, 75, 0),
  (20, 76, 0),
  (20, 77, 0),
  (20, 78, 1),
  (21, 79, 0),
  (21, 80, 1),
  (21, 81, 0),
  (21, 82, 1),
  (22, 83, 0),
  (22, 84, 1),
  (23, 85, 1),
  (23, 86, 0),
  (24, 87, 0),
  (24, 88, 0),
  (24, 89, 1),
  (25, 90, 0),
  (25, 91, 0),
  (25, 92, 1),
  (25, 93, 0),
  (26, 94, 0),
  (26, 95, 1),
  (27, 96, 0),
  (27, 97, 1),
  (28, 98, 1),
  (28, 99, 0),
  (29, 100, 1),
  (29, 99, 0),
  (30, 84, 1),
  (30, 85, 0),
  (30, 86, 0),
  (31, 87, 1),
  (31, 88, 0),
  (32, 89, 1),
  (32, 90, 0),
  (32, 91, 0),
  (32, 92, 0),
  (33, 76, 1),
  (33, 77, 0),
  (33, 78, 0),
  (33, 79, 0),
  (34, 80, 1),
  (34, 81, 0),
  (35, 82, 1),
  (35, 83, 0),
  (36, 84, 1),
  (36, 85, 0);

INSERT INTO roles (role_name)
VALUES
  ('master'),
  ('admin'),
  ('guest');

INSERT INTO users (user_name, user_password, role_id)
VALUES
  ('dbmaster', '$2a$04$0MaUGArY5R/hgTlFriR0xu8pNf/NbXnBD5CGqKfdiF0w711QVWg/i', 1),
  ('dbadmin', '$2a$04$SxJ8W5T/Lw7qsGOls8x0Zu53NoihHkjE0IsT/8K2YT.2pZWT4LH1.', 2);

INSERT INTO quiz (quiz_name, quiz_password, topic_id, random_shuffle, time_limit)
VALUES
  ('first-quiz', '$2a$04$UV/ThrMOb5OAb9dyPtImVOHwBQRgm0kiJaYhINaNk3pWgCdjam4ke', 1, 1, 120),
  ('second-quiz', '$2a$04$ScHeRxIF2ijVQ7mBufN0B.9.Jf/.bsIiubfpNkqSG7nWJRApdoWoe', 2, 0, 240),
  ('test-quiz-topic-1', '$2a$04$BCOoXHVlyOVkWFPNUZ85GeBkwZXtKHxU/uS820X2/XNRWwn5SQdii', 1, 1, 350),
  ('test-quiz2-topic-1', '$2a$04$wa6RL2CJSVhIvPETlzB2Vu.nvThmCQlaL5zrz7l9hSO.jhnk4KXzS', 1, 0, 530),
  ('test-quiz3-topic-1', '$2a$04$mYoLBa50LeoQy7elzZ6iGeC2zNjFuvE4Vv7RcffvZIB/WdiDJIT.S', 1, 0, 110);

INSERT INTO quiz_questions (quiz_id, question_id)
VALUES
  (1, 1), (1, 2), (1, 3), (1, 4), (1, 5),
  (1, 6), (1, 7), (1, 8), (1, 9), (1, 10),
  (1, 11), (1, 12), (1, 13), (1, 14), (1, 15),
  (1, 16), (1, 17), (1, 18), (1, 19),
  (2, 20), (2, 21), (2, 22), (2, 23), (2, 24),
  (2, 25), (2, 26), (2, 27), (2, 28), (2, 29),
  (2, 30), (2, 31), (2, 32), (2, 33), (2, 34),
  (2, 35), (2, 36), (2, 37);

INSERT INTO user_quiz_stats (user_id, quiz_id, score, solved_correct, solved_incorrect)
VALUES
  (2, 1, 16, 10, 11);