var mysql = require('mysql');

var params = {
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: 'quiz_db',
    multipleStatements: true,
    typeCast: function castField(field, useDefaultTypeCasting) {
        if ((field.type === "BIT") && (field.length === 1)) {

            var bytes = field.buffer();
            return (bytes[0] === 1);
        };

        return (useDefaultTypeCasting());
    }
};

var connection = mysql.createConnection(params);

connection.connect(function (error) {
    if (error) {
        throw error;
    };

    console.log('Connected to database');
});

module.exports = connection;
