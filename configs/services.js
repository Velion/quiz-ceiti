var express = require('express');
var Q = require('q');
var bcrypt = require('bcryptjs');
var moment = require('moment');
var connection = require('../configs/connection');

// setting moment.js locale to romanian
moment.locale('ro');

function queryDashboardData() {
    return Q.promise(function (resolve, reject, notify) {
        let query = `SELECT COUNT(topic_id) as dashboard_data FROM topics 
				UNION SELECT COUNT(question_id) as dashboard_data FROM questions 
				UNION SELECT COUNT(topic_id) as dashboard_data FROM quiz
				UNION SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = ?`;

        connection.query(query, ['quiz_db'], function (error, rows, fields) {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            };
        });
    });
};

function queryTopicsList() {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'SELECT topic_id as id, topic_name as topic, knowledge_of as knowledgeOf FROM quiz_db.topics';

        connection.query(query, function (error, rows, fields) {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            };
        });
    });
};

function queryQuizes(quizId, topicId) {
    return Q.promise(function (resolve, reject, notify) {
        let query = `SELECT
                quiz.quiz_id AS id,
                quiz.quiz_name AS name,
                quiz.quiz_password AS password,
                quiz.topic_id AS topicId,
                topics.topic_name AS topicName,
                topics.knowledge_of AS knowledgeOf,                
                quiz.random_shuffle AS shuffle,
                quiz.time_limit AS time,
                quiz.created_on AS createdOn
                FROM quiz_db.quiz
                INNER JOIN quiz_db.topics ON quiz.topic_id = topics.topic_id`;

        let params = [];

        if (quizId) {
            query += ' AND quiz.quiz_id = ?';
            params.push(quizId);
        };

        if (topicId) {
            query += ' AND topics.topic_id = ?'
            params.push(topicId);
        };

        connection.query(query, params, function (error, rows, fields) {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            };
        });
    });
};

function queryQuizQuestions(quizId) {
    return Q.promise(function (resolve, reject, notify) {
        let query = `
        SELECT
            DISTINCT
            @questionId := questions.question_id                      AS id,
            questions.question_name                                   AS name,
            question_types.question_type_name                         AS type,
            questions.points                                          AS points,
            GROUP_CONCAT(DISTINCT answers.answer_name SEPARATOR '~~') AS answers,
            (SELECT GROUP_CONCAT(DISTINCT answers.answer_name SEPARATOR '~~') AS correct_answers
            FROM answers
                JOIN question_answers
                ON answers.answer_id = question_answers.answer_id
                    AND question_answers.is_correct = 1
                    AND
                    question_answers.question_id = @questionId)       AS correctAnswers
            FROM quiz_db.questions
            LEFT JOIN quiz_db.question_answers ON questions.question_id = question_answers.question_id
            LEFT JOIN quiz_db.answers ON question_answers.answer_id = answers.answer_id
            JOIN quiz_db.question_types ON questions.question_type_id = question_types.question_type_id
            JOIN quiz_db.quiz_questions ON quiz_questions.question_id = questions.question_id
            JOIN quiz_db.quiz ON quiz_questions.quiz_id = quiz.quiz_id AND quiz.quiz_id = ?
            GROUP BY (questions.question_id)`;

        connection.query(query, [quizId], function (error, rows, fields) {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            };
        });
    });
};

function initQuizes(quizesDb) {
    return Q.promise(function (resolve, reject, notify) {
        var quizes = [];
        for (var i = 0; i < quizesDb.length; i++) {
            quizes[i] = {
                id: quizesDb[i].id,
                name: quizesDb[i].name,
                topicId: quizesDb[i].topicId,
                topicName: quizesDb[i].topicName,
                knowledgeOf: quizesDb[i].knowledgeOf,
                randomShuffle: quizesDb[i].shuffle,
                timeLimit: moment.duration(quizesDb[i].time, 'seconds').humanize(),
                timeSeconds: quizesDb[i].time,
                createdOn: moment(quizesDb[i].createdOn).format('LLL')
            };
        };

        if (quizes) {
            resolve(quizes);
        } else {
            reject(new Error('No quizes found'));
        };
    });
};

function queryAddUser(userName) {
    return Q.promise(function (resolve, reject, notify) {
        var userId = 0;
        // multiple queries are executed
        // (1) the first one inserts the new user if not already in database
        // (2) the second one selects the id of the user tried to insert
        var query = 'INSERT IGNORE INTO users (user_name, role_id) VALUES (?, 3); '
            + 'SELECT user_id as userId FROM users WHERE user_name = ?';

        // since both queries use the same parameter, it is written twice
        connection.query(query, [userName, userName], function (error, rows, fields) {
            if (error) {
                reject(error);
            } else {
                resolve(rows[1][0].userId);
            };
        });
    });
};

function queryAddUserStats(quizStats, userId) {
    return Q.promise(function (resolve, reject, notify) {
        var query = 'INSERT INTO user_quiz_stats (user_id, quiz_id, score, solved_correct, solved_incorrect, mark, skipped)'
            + 'VALUES (?, ?, ?, ?, ?, ?, ?)';

        var params = [
            userId,
            quizStats.quizId,
            quizStats.score,
            quizStats.correct,
            quizStats.incorrect,
            quizStats.mark,
            quizStats.skipped
        ];

        connection.query(query, params, function (error, rows, fields) {
            if (error) {
                reject(error);
            } else {
                resolve(true);
            };
        });
    });
};

function queryQuizPassword(quizId) {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'SELECT quiz_password as hash FROM quiz WHERE quiz_id = ?';

        connection.query(query, [quizId], function (error, rows, fields) {
            if (error) {
                reject(new Error(error));
            } else {
                var hash = '';
                if (rows[0].hash) {
                    hash = rows[0].hash;
                };
                resolve(hash);
            };
        });
    });
};

function verifyQuizPassword(quizPassword, hash) {
    return Q.promise(function (resolve, reject, notify) {
        bcrypt.compare(quizPassword, hash, function (err, res) {
            if (res) {
                resolve(true);
            } else {
                reject(new Error('Password does not match'));
            };
        });
    });
};

function queryQuestion(questionId) {
    return Q.promise(function (resolve, reject, notify) {
        let query = `
           SELECT
            question_name                                             AS name,
            question_type_name                                        AS type,
            question_types.question_type_id                           AS typeId,
            points,
            group_concat(DISTINCT answers.answer_name SEPARATOR '~~') AS answers,
            (SELECT group_concat(
                DISTINCT answers.answer_name
                SEPARATOR '~~'
            )
            FROM answers
                JOIN question_answers
                ON answers.answer_id = question_answers.answer_id
                    AND question_answers.is_correct = 1
                    AND question_answers.question_id = ?)             AS correctAnswers
            FROM questions
            JOIN question_types
                ON questions.question_type_id = question_types.question_type_id
            JOIN quiz_db.question_answers
                ON questions.question_id = question_answers.question_id
            JOIN quiz_db.answers
                ON question_answers.answer_id = answers.answer_id
                AND questions.question_id = ?;`;

        connection.query(query, [questionId, questionId], function (error, rows, fields) {
            if (error) {
                reject(error);
            } else {
                resolve(rows[0]);
            };
        });
    });
};

function queryQuestionTypes() {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'SELECT question_type_id as id, question_type_name as name FROM question_types';

        connection.query(query, function (error, rows, fields) {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            };
        });
    });
};

function queryAddQuiz(name, password, topicId, random, time) {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'INSERT INTO quiz (quiz_name, quiz_password, topic_id, random_shuffle, time_limit) VALUES (?, ?, ?, ?, ?)';

        connection.query(query, [name, password, topicId, random, time], function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result.insertId);
            };
        });
    });
};

function queryRemoveQuizes(quizes) {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'DELETE FROM quiz WHERE quiz_id IN (?)';

        connection.query(query, [quizes], function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(true);
            };
        });
    });
};

function queryUpdateQuiz(quizData) {
    return Q.promise(function (resolve, reject, notify) {
        let query = `
        UPDATE quiz SET
            quiz.quiz_name = ?,
            quiz.topic_id = ?,
            quiz.random_shuffle = ?,
            quiz.time_limit = ?
        WHERE quiz.quiz_id = ?
        `;

        connection.query(query, [
            quizData.name,
            quizData.topicId,
            quizData.random,
            quizData.time,
            quizData.id
        ], function (error, results) {
            if (error) {
                reject(error);
            } else {
                resolve(results.changedRows);
            };
        });
    });
};

function queryAddQuestion(name, type, points) {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'INSERT INTO questions (question_name, question_type_id, points) VALUES (?, ?, ?)';

        connection.query(query, [name, type, points], function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result.insertId);
            };
        });
    });
};

function queryAddQuestionToQuiz(quizId, questionId) {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'INSERT INTO quiz_questions(quiz_id, question_id) VALUES (?, ?)';

        connection.query(query, [quizId, questionId], function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result.insertId);
            };
        });
    });
};

function queryRemoveQuestion(questionId) {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'DELETE FROM questions WHERE question_id = ?';

        connection.query(query, [questionId], function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(true);
            };
        });
    });
};

function queryAddAnswer(answer) {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'INSERT INTO answers(answer_name) VALUES (?)';

        connection.query(query, [answer], function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result.insertId);
            };
        });
    });
};

function queryAddAnswerToQuestion(questionId, answerId, isCorrect) {
    return Q.promise(function (resolve, reject, notify) {
        let query = 'INSERT INTO question_answers(question_id, answer_id, is_correct) VALUES (?, ?, ?)';

        connection.query(query, [questionId, answerId, isCorrect], function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result.insertId);
            };
        });
    });
};

function queryRemoveAnswers(questionId) {
    return Q.promise(function (resolve, reject, notify) {
        let query = `
        DELETE answers FROM answers INNER JOIN question_answers
            ON answers.answer_id = question_answers.answer_id AND question_id = ?;`;

        connection.query(query, [questionId], function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(true);
            };
        });
    });
};

function queryUpdateQuestion(questionData) {
    return Q.promise(function (resolve, reject, notify) {
        let query = `
        UPDATE questions SET
            questions.question_name = ?,
            questions.question_type_id = ?,
            questions.points = ?
        WHERE question_id = ?;`;

        connection.query(query, [
            questionData.name,
            questionData.typeId,
            questionData.points,
            questionData.id
        ], function (error, results) {
            if (error) {
                reject(error);
            } else {
                resolve(results.changedRows);
            };
        });
    });
};

function getTotalWeekUsers() {
    return Q.promise(function (resolve, reject, notify) {
        let query = `
        SELECT
            date(user_quiz_stats.solved_on) AS 'day',
            count(user_quiz_stats.quiz_id)  AS 'total'
        FROM user_quiz_stats
        WHERE user_quiz_stats.solved_on >= DATE(NOW()) - INTERVAL 7 DAY
        GROUP BY date(user_quiz_stats.solved_on)
        ORDER BY solved_on DESC`;

        connection.query(query, function (error, rows, fields) {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            };
        });
    });
};

function initTotalWeekUsers(weekData) {
    return Q.promise(function (resolve, reject, notify) {
        var weekDashboardData = [];
        weekData.forEach(function (dailyData) {
            weekDashboardData.push({
            	day: moment(dailyData.day).format('dddd'),
            	total: dailyData.total
            });
        });

        if (weekDashboardData) {
            resolve(weekDashboardData);
        } else {
            reject(new Error('No week data found'));
        };
    });
};

module.exports = {
    quizes: {
        queryQuizPassword: queryQuizPassword,
        verifyQuizPassword: verifyQuizPassword,
        queryQuizes: queryQuizes,
        initQuizes: initQuizes,
        queryAddQuiz: queryAddQuiz,
        queryRemoveQuizes: queryRemoveQuizes,
        queryQuizQuestions: queryQuizQuestions,
        queryQuestion: queryQuestion,
        queryQuestionTypes: queryQuestionTypes,
        queryAddQuestion: queryAddQuestion,
        queryRemoveQuestion: queryRemoveQuestion,
        queryAddQuestionToQuiz: queryAddQuestionToQuiz,
        queryAddAnswer: queryAddAnswer,
        queryAddAnswerToQuestion: queryAddAnswerToQuestion,
        queryRemoveAnswers: queryRemoveAnswers,
        queryUpdateQuestion: queryUpdateQuestion,
        queryUpdateQuiz: queryUpdateQuiz
    },
    topics: {
        queryTopicsList: queryTopicsList
    },
    dashboard: {
        queryDashboardData: queryDashboardData,
        getTotalWeekUsers: getTotalWeekUsers,
        initTotalWeekUsers: initTotalWeekUsers
    },
    users: {
        queryAddUser: queryAddUser,
        queryAddUserStats: queryAddUserStats
    }
};