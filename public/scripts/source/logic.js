$(document).ready(function () {
    'use strict';

    quizData = JSON.parse(quizData);

    console.log(quizData);
    var questions = quizData.questions;
    var topicData = quizData.topicData;
    var quizTime = quizData.quizTime;
    var quizId = quizData.quizId;

    var configElems = {
        $progress: '.progress.quiz-progress .determinate',
        $questionsNr: '.quiz-question-nr',
        $questionsTotal: '.quiz-questions-total',
        $questionName: '#question-title',
        $topicName: '.quiz-topic-name',
        $textUserAnswer: '#quiz-text-input',
        $checkedUserAnswer: '.option-input:checked',
        $startOverBtn: '#quiz-startover',
        $chart: '#chart',
        $choices: '#choices',
        $quiz: '#quiz',
        $nextQuestionBtn: '#quiz-next-btn',
        $quizData: '#quiz-data',
        $prevBtn: '#quiz-prev-btn',
        $skipBtn: '#quiz-skip-btn',
        $abandonBtn: '#quiz-abandon',
        $forceFinishBtn: '#quiz-force-finish',
        $buttonCollapse: '.button-collapse'
    };

    // Initialize collapse button
    $(configElems.$buttonCollapse).sideNav();

    // hide page then display when loaded or after timeout 
    $('header, main, footer').css('display', 'none');
    setTimeout(function () {
        $('#loader-wrapper').fadeOut(function () {
            $(this).remove();
            $('header, main, footer').fadeIn(200);
        });
    }, 300);

    Quiz = {
        // Current question index in the array, starting with 0
        currentIndex: 0,
        // Current score and correct, increments when the user chooses the right answer
        currentScore: 0,
        correct: 0,
        config: {},
        questions: [],
        selectedAnswers: [],
        skippedQuestions: [],
        verifiedAnswers: [],
        isRepeatingSkippedQuestions: false,

        init: function (config) {
            Quiz.config = config;

            Quiz.questions = questions;
            // If they reached the final question of the quiz
            // Calculate their final score

            if (Quiz.currentIndex + 1 > Quiz.questions.length) {
                // Quiz.currentIndex = Quiz.skippedQuestions[0];
                // Quiz.skippedQuestions.shift();
                // Quiz.renderTitle();
                // Quiz.renderChoices();
                console.log(Quiz.selectedAnswers);
                Quiz.verifyGivenAnswers();
                Quiz.renderFinalScore();
            } else {
                Quiz.renderTitle();
                Quiz.renderChoices();
            };
        },

        renderTitle: function () {
            // Get precompiled template
            var template = Quiz.config.titleTemplateEl;

            console.log(Quiz.questions[Quiz.currentIndex]);
            var context = {
                title: Quiz.questions[Quiz.currentIndex].name,
                type: Quiz.questions[Quiz.currentIndex].type
            };

            // Display the question title
            $(Quiz.config.questionTitleEl).html(template(context));

            // change question nr in view
            $(configElems.$progress).css('width', (Quiz.currentIndex + 1) / Quiz.questions.length * 100 + "%");
            $(configElems.$questionsNr).html(Quiz.currentIndex + 1);
            $(configElems.$questionsTotal).html(Quiz.questions.length);
        },

        renderChoices: function () {
            var template = Quiz.config.choicesTemplateEl;

            var context = {
                choices: Quiz.questions[Quiz.currentIndex].answers,
                correctChoices: Quiz.questions[Quiz.currentIndex].correctAnswers,
                type: Quiz.questions[Quiz.currentIndex].type
            };

            // Display the question choices
            $(Quiz.config.choicesEl).html(template(context));
        },

        handleQuestion: function (skipped) {

            var currentAnswers = [],
                id = Quiz.questions[Quiz.currentIndex].id,
                questionType = Quiz.questions[Quiz.currentIndex].type;

            // add user inputs to array for later use  
            if (questionType == 'write') {
                // add user text input
                currentAnswers.push($.trim($(configElems.$textUserAnswer).val()));
            } else if (questionType == 'truefalse') {
                $('.quiz-choice-item .switch input').each(function () {
                    // if toggle is not off add inputs to array
                    if (!$(this).prop('checked')) {
                        currentAnswers.push($(this).prop('checked'));
                    };
                });
            } else {
                // add user selected choices
                $(configElems.$checkedUserAnswer).each(function () {
                    currentAnswers.push($(this).val());
                });
            };

            // Quiz.selectedAnswers.push({ id: id, answers: currentAnswers });
            // Increment the current index so it can advance to the next question
            // And Re-render everything.
            if (!skipped) {
                Quiz.selectedAnswers.push({ id: id, answers: currentAnswers });
                Quiz.currentIndex += 1;
            }

            Quiz.init(Quiz.config);
        },

        previousQuestion: function () {
            if (Quiz.currentIndex > 0) {
                Quiz.currentIndex -= 1;
                Quiz.init(Quiz.config);
            };
        },

        renderFinalScore: function () {
            var template = Quiz.config.finalScoreTemplateEl;

            // hide footer quiz progress on render final-score 
            $('#footer-progress').html('');
            $('footer').css('padding-top', 0);

            var correct = Quiz.correct,
                totalScore = Quiz.currentScore,
                total = Quiz.questions.length,
                skipped = Quiz.skippedQuestions.length,
                incorrect = total - correct - skipped,
                percent = Math.floor(correct / total * 100),
                mark = (percent >= 95) ? 10 : (percent >= 80) ? 9 : (percent >= 60) ? 8 :
                    (percent >= 50) ? 7 : (percent >= 30) ? 6 : (percent >= 15) ? 5 :
                        (percent >= 10) ? 4 : (percent >= 5) ? 3 : (percent > 0) ? 2 : 1;

            var context = {
                correct: correct,
                totalScore: totalScore,
                questionsLength: total,
                skipped: skipped,
                incorrect: incorrect,
                percent: percent,
                mark: mark,
                verifiedAnswers: Quiz.verifiedAnswers
            };

            // stop the timer when results are shown
            timer.stop();

            $(Quiz.config.quizEl).html(template(context));

            // draw the graph along with the quiz results
            Quiz.drawFinalScoreChart(context);

            var requestParams = {
                userName: quizData.quizUser,
                isGuest: true,
                mark: mark,
                score: totalScore,
                correct: correct,
                incorrect: incorrect,
                skipped: skipped,
                quizId: quizId
            };

            $.post('/data/users', requestParams)
                .done(function (res) {
                    console.log(res);
                })
                .fail(function (error) {
                    console.log(error.status, error.statusText);
                });

            // start over the quiz
            $(configElems.$startOverBtn).click(function () {
                setTimeout(function () {
                    window.location.reload();
                }, 100);
            });
        },

        drawFinalScoreChart: function (context) {
            var correct = context.correct,
                total = context.questionsLength,
                skipped = context.skipped,
                incorrect = context.incorrect,
                percent = context.percent,
                score = context.totalScore;

            // add text to center of the doughnut chart
            Chart.pluginService.register({
                beforeDraw: function (chart) {
                    var width = chart.chart.width,
                        height = chart.chart.height,
                        ctx = chart.chart.ctx;

                    ctx.restore();
                    var fontSize = (height / 114).toFixed(2);
                    ctx.font = fontSize + 'em sans-serif';
                    ctx.textBaseline = 'middle';
                    ctx.fillStyle = '#325d88';

                    var text = percent + '%',
                        textX = Math.round((width - ctx.measureText(text).width) / 2),
                        textY = height / 2;

                    ctx.fillText(text, textX, textY);
                    ctx.save();
                }
            });

            var chartCtx = $(configElems.$chart);

            var graphData = [
                correct,
                incorrect,
                skipped
            ];

            // draw chart with given context
            var chart = new Chart(chartCtx, {
                type: 'doughnut',
                data: {
                    labels: [
                        "Corecte",
                        "Incorecte",
                        "Fara raspuns"
                    ],
                    datasets: [
                        {
                            data: graphData,
                            backgroundColor: [
                                "#36A2EB",
                                "#FF6384",
                                "#FFCE56"
                            ],
                            hoverBackgroundColor: [
                                "#36A2EB",
                                "#FF6384",
                                "#FFCE56"
                            ]
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    }
                }
            })
        },

        verifyGivenAnswers: function () {
            for (var i = 0; i < Quiz.questions.length; i++) {
                var correctChoicesArr = Quiz.questions[i].correctAnswers,
                    selectedAnswersArr = (Quiz.selectedAnswers[i]) ? Quiz.selectedAnswers[i].answers : [];

                var isCorrect = _.isEqual(correctChoicesArr.sort(), selectedAnswersArr.sort());
                if (isCorrect) {
                    Quiz.currentScore += parseInt(Quiz.questions[i].points);
                    Quiz.correct++;
                } else {
                    console.log('not equal');
                };

                Quiz.verifiedAnswers.push({
                    id: Quiz.questions[i].id,
                    question: Quiz.questions[i].question,
                    givenAnswers: selectedAnswersArr,
                    isCorrect: isCorrect
                });

                // if (Quiz.questions[i].id != Quiz.selectedAnswers[i].id) {
                //     Quiz.skippedQuestions.push(_.difference(Quiz.questions.sort(), Quiz.selectedAnswers.sort()));
                // }
            };
        }
    };

    // Providing a config object just so 
    // when the element names change for some reason 
    // there is no need to change the whole element names
    Quiz.init({
        choicesTemplateEl: Handlebars.templates['choices.tmpl'],
        titleTemplateEl: Handlebars.templates['title.tmpl'],
        finalScoreTemplateEl: Handlebars.templates['final-score.tmpl'],
        questionTitleEl: configElems.$questionName,
        choicesEl: configElems.$choices,
        quizEl: configElems.$quiz
    });

    handleTimer();

    // Handle question logic on 'next button' click
    $(configElems.$nextQuestionBtn).on('click', function () {
        var questionType = Quiz.questions[Quiz.currentIndex].type;
        if (questionType == 'write') {
            if ($(configElems.$textUserAnswer).val().length == 0) {
                showToast('Nu ati scris raspunsul', 1500);
                $(configElems.$textUserAnswer).focus();
                return;
            };
        } else if (questionType == 'truefalse') {
            // do nothing if truefalse because it has a default value
        } else {
            if ($(configElems.$checkedUserAnswer).length == 0) {
                showToast('Nu ati selectat nici un raspuns', 1500);
                return;
            };
        };

        // fade question in and out
        $(configElems.$quizData).fadeOut(100, function () {
            Quiz.handleQuestion();
        });
        $(configElems.$quizData).fadeIn(300);
    });

    // previous quiz question
    $(configElems.$prevBtn).on('click', Quiz.previousQuestion);

    // skip question to next
    $(configElems.$skipBtn).on('click', function () {
        // fade question in and out
        $(configElems.$quizData).fadeOut(300, function () {
            // Quiz.skippedQuestions.push(Quiz.currentIndex);
            // move skipped question to end of questions array
            Quiz.questions.push(Quiz.questions.splice(Quiz.currentIndex, 1)[0]);

            Quiz.handleQuestion(true);
            console.log(Quiz.questions);

        });
        $(configElems.$quizData).fadeIn(300);
    });

    // abandon quiz and return to topics selection
    $(configElems.$abandonBtn).on('click', function () {
        confirmAction('Doriți să abandonați testul?', function (confirmResult) {
            if (confirmResult) {
                window.location.href = '../';
            };
        });
    });

    // force quiz to end with current results only
    $(configElems.$forceFinishBtn).click(function (e) {
        e.preventDefault();

        confirmAction('Doriți să finisați testul?', function (confirmResult) {
            if (confirmResult) {
                for (var i = Quiz.currentIndex; i < Quiz.questions.length; i++) {
                    Quiz.skippedQuestions.push(Quiz.currentIndex);
                };

                Quiz.currentIndex = Quiz.questions.length;
                Quiz.init(Quiz.config);
            };
        }, true);
    });

    function handleTimer() {
        // start timer
        timer.init(quizTime);

        var countdownNumberEl = $('.quiz-countdown-number')[0];
        var countdown = timer.getTime();

        $('.quiz-countdown svg circle.quiz-countdown-circle').css('animation', 'countdown ' + countdown + 's linear infinite forwards');
        countdownNumberEl.textContent = timer.getFormatedTime();

        timer.counter = setInterval(function () {
            timer.count();

            countdown = --countdown < 0 ? 60 : countdown;
            countdownNumberEl.textContent = timer.getFormatedTime();
        }, 1000);
    };

    function showToast(message, duration) {
        if (!$('.toast').length) {
            Materialize.toast(message, duration);
        };
    };

    function confirmAction(confirmMessage, callback, autoclose) {
        $('#quiz-confirm.modal .quiz-confirm-message').text(confirmMessage);
        $('#quiz-confirm.modal').modal();

        autoclose = autoclose || false;

        $('#quiz-confirm-no').on('click', function (e) {
            e.preventDefault();
            $('#quiz-confirm.modal').modal('close');
            callback(false);
        });

        $('#quiz-confirm-yes').off('click').on('click', function (e) {
            e.preventDefault();
            if (autoclose) {
                $('#quiz-confirm.modal').modal('close');
            };
            callback(true);
        });
    };
});