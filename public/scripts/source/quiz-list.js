$(document).ready(function () {
    'use strict';

    quizList = JSON.parse(quizList);
    console.log(quizList);

    var quizTime = 0;
    var quizSelectedId = 0;
    var toggleSort = false;

    var configElems = {
        $quizList: '#quiz-list',
        items: {
            $quizItems: '.quiz-item',
            $quizId: '.quiz-id',
            $quizName: '.quiz-name',
            $quizTime: '.quiz-time',
            $quizDate: '.quiz-date'
        },
        $quizSearch: '#quiz-search',
        $searchInputClear: '.search-input-clear',
        $searchNoResults: '.search-no-results',
        $sortBtn: '#quiz-sort .quiz-sort-btn',
        $quizUser: '#quiz-user',
        $quizPassword: '#quiz-password',
        $quizPasswordLabel: '.quiz-password-label',
        $modal: '.content-wrapper .modal',
        $modalForm: '#quiz-modal-form',
        $modalTrigger: '.quiz-modal-trigger',
        $buttonCollapse: '.button-collapse'
    };

    // Initialize collapse button
    $(configElems.$buttonCollapse).sideNav();

    // if the topic does not have any topics display message
    if ($(configElems.items.$quizItems).length == 0) {
        $(configElems.$searchNoResults).fadeIn(0);
    };

    $('#quiz-search').on('keyup', _.debounce(function () {
        liveSearch();
    }, 200));

    function liveSearch() {
        var $input;
        var filter;
        var $ul;
        var $li;
        var $a;
        var countHidden = 0;

        $input = $('#quiz-search');
        filter = $input.val().toUpperCase();
        $ul = $('#quiz-list');
        $li = $($ul).find('li');

        for (var i = 0; i < $li.length; i++) {
            $a = $($li[i]).find('a.quiz-name')[0];
            if ($($a).text().toUpperCase().indexOf(filter) > -1) {
                $($li[i]).css('display', '');
            } else {
                $($li[i]).css('display', 'none');
            };
        };

        countHidden = $('#quiz-list li:hidden').length;

        if (countHidden == $li.length) {
            $(configElems.$searchNoResults).fadeIn('slow');
        } else {
            $(configElems.$searchNoResults).fadeOut(0);
        };

        $($input).parent().find('i.search-clear').on('click', function () {
            $($input).val('');
            $($input).keyup();
        });
    };

    $(configElems.$searchInputClear).on('click', function () {
        $(configElems.$quizSearch).val('');
        $(configElems.$searchNoResults).fadeOut(0);
        $(configElems.items.$quizItems).each(function () {
            $(this).fadeIn();
        });
    });

    $(configElems.$sortBtn).click(function () {
        var sortBy = $(this).data('sort');
        var $propElem = '';
        if (sortBy == 'name') {
            $propElem = $(configElems.items.$quizName);
        } else if (sortBy == 'time') {
            $propElem = $(configElems.items.$quizTime);
        } else if (sortBy == 'date') {
            $propElem = $(configElems.items.$quizDate);
        };

        toggleSort = !toggleSort;

        sortListByProp($propElem);
    });

    function sortListByProp(propElem) {
        var $sortedItems = _.sortBy($(configElems.items.$quizItems), [function (quiz) {
            return $(quiz.children).find(propElem).text();
        }]);

        $sortedItems.reverse();

        rebuildList($sortedItems);
    };

    function rebuildList($sortedItems) {
        $(configElems.items.$quizItems).detach();
        $($sortedItems).fadeOut('fast').appendTo(configElems.$quizList).fadeIn('fast');
    };

    $(configElems.$quizList).on('click', configElems.$modalTrigger, function (e) {
        e.preventDefault();
        $(configElems.$modal).modal();
        quizSelectedId = parseInt($(e.target).closest(configElems.items.$quizItems).find(configElems.items.$quizId).text());
        // get the selected quiz time by searching the array, knowing the quiz id
        quizTime = parseInt(_.find(quizList, ['id', quizSelectedId]).timeSeconds);
    });

    $(configElems.$modal).on('submit', configElems.$modalForm, function (e) {
        e.preventDefault();
        var quizPasswordInput = $(configElems.$quizPassword).val().trim();
        var quizUser = $(configElems.$quizUser).val().trim();
        console.log(quizPasswordInput);

        var topicData = JSON.parse(localStorage.getItem('topicData'));

        var reqestParams = {
            topicData: topicData,
            quizId: quizSelectedId,
            quizPassword: quizPasswordInput,
            quizUser: quizUser,
            quizTime: quizTime
        };

        $.get('/quiz', reqestParams)
            .done(function (res, statusText, xhr) {
                console.log(res, statusText, xhr);

                if (res.status == 400) {
                    console.log(res);
                    $(configElems.$quizPasswordLabel).attr('data-error', 'Parola incorecta! Mai incearca.');
                    $(configElems.$quizPassword).addClass('invalid');
                    $(configElems.$quizPassword).prop('aria-invalid', 'true');
                } else {
                    // navigate to /quiz                    
                    window.location.href = '/quiz';
                };
            }).fail(function (err) {
                console.log('Request failed: ', err.status);
            });
    });
});

