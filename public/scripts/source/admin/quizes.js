$(document).ready(function () {
    'use strict';

    // parse dashboard data retrieved from server and stringified with handlebars helpers
    // dashboardData = JSON.parse(dashboardData);
    // topicsList = JSON.parse(topicsList);
    quizes = JSON.parse(quizes);
    console.log(quizes);
    loadSidenav();

    $('.button-collapse').sideNav();
    $('.collapsible').collapsible();
    $('.modal').modal();

    var sort = new Tablesort(document.getElementById('quizes-table'));

    function loadSidenav() {
        var template = Handlebars.templates['sidenav.tmpl'];
        $('#sidenav-wrapper').html(template({ pageTitle: 'Manage Quizes' }));
    };

    paginate();

    $('#select-all').on('change', function () {
        var checkboxes = $(this).closest('table').find(':checkbox');
        if ($(this).is(':checked')) {
            checkboxes.prop('checked', true);
        } else {
            checkboxes.prop('checked', false);
        };
    });

    $('input[type="checkbox"]').on('change', function () {
        var $removeQuiz = $('#remove-quiz');

        if ($('input[type="checkbox"]:checked').length > 0) {
            $($removeQuiz).css('visibility', 'visible');
        } else {
            $($removeQuiz).css('visibility', 'hidden');
        };
    });

    $('#remove-quiz').on('click', function () {
        var selectedQuizElems = $('input[type="checkbox"]:checked');
        var selectedQuizes = [];
        $.each(selectedQuizElems, function (i, chkbox) {
            var id = parseInt($(chkbox).closest('tr').data('id'));
            selectedQuizes.push(id);
        });
        console.log(selectedQuizes);

        confirmAction('Doriti sa stergeti testele selectate?', function (confirmResult) {
            if (confirmResult) {
                $.ajax({
                    url: '/admin/quizes/',
                    type: 'DELETE',
                    data: { quizes: JSON.stringify(selectedQuizes) }
                }).done(function (res) {
                    console.log(res);
                    $(selectedQuizElems).closest('tr').remove();
                }).fail(function (error) {
                    console.log(error);
                });
            };
        }, true);
    });

    $('#add-quiz-form').on('submit', function (e) {
        e.preventDefault();

        var formParams = $(this).serializeArray();

        $.post('/admin/quizes/', { quizName: formParams[0].value, quizPassword: formParams[1].value })
            .done(function (res) {
                console.log(res);
                window.location.href = '/admin/quizes/' + res.quizId;
            })
            .fail(function (error) {
                console.log(error);
            });
    });

    $('#quiz-search').on('keyup', _.debounce(function () {
        liveSearch();
    }, 100));

    function liveSearch() {
        var $input, filter, $table, $tr, $td;
        $input = $('#quiz-search');
        filter = $input.val().toUpperCase();
        $table = $('#quizes-table');
        $tr = $table.find('tr');

        for (var i = 0; i < $tr.length; i++) {
            $td = $($tr[i]).find('td')[1];
            if ($td) {
                if ($($td).find('a').text().toUpperCase().indexOf(filter) > -1) {
                    $($tr[i]).css('display', '');
                } else {
                    $($tr[i]).css('display', 'none');
                };
            };
        };

        $($input).parent().find('i.search-clear').on('click', function () {
            $($input).val('');
            $($input).keyup();
        });
    };

    function paginate() {
        var shownRows = 11;
        var $tr = $('tbody tr');
        var totalRows = $tr.length;
        var pages = 0;

        if (totalRows % shownRows == 0) {
            pages = totalRows / shownRows;
        };

        if (totalRows % shownRows >= 1) {
            pages = totalRows / shownRows;
            pages++;
            pages = Math.floor(pages++);
        };

        $('#pager').materializePagination({
            lastPage: pages,
            firstPage: 1,
            align: 'right',
            urlParameter: 'page',
            useUrlParameter: false,
            onClickCallback: function (page) {
                $tr.hide();
                var temp = page - 1;
                var start = temp * shownRows;

                for (var i = 0; i < shownRows; i++) {
                    $tr.eq(start + i).show();
                };
            }
        });
    };

    function confirmAction(confirmMessage, callback, autoclose) {
        $('#quiz-confirm.modal .quiz-confirm-message').text(confirmMessage);
        $('#quiz-confirm.modal').modal();

        autoclose = autoclose || false;

        $('#quiz-confirm-no').on('click', function (e) {
            e.preventDefault();
            $('#quiz-confirm.modal').modal('close');
            callback(false);
        });

        $('#quiz-confirm-yes').off('click').on('click', function (e) {
            e.preventDefault();
            if (autoclose) {
                $('#quiz-confirm.modal').modal('close');
            };
            callback(true);
        });
    };
});