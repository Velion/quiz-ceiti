$(document).ready(function () {
    'use strict';

    window.history.pushState('', document.title, window.location.pathname);

    quiz = JSON.parse(quiz);
    topics = JSON.parse(topics);
    console.log(quiz, topics);

    loadSidenav();
    $('.collapsible').collapsible();
    $('.button-collapse').sideNav();
    $('.modal').modal();

    $('#topics-wrapper ul li > input[name="quiz-topic"]').each(function () {
        var radioInput = $(this);
        if (radioInput.is(':checked')) {
            $('#topics-wrapper').animate({
                scrollTop: radioInput.offset().top - $('#topics-wrapper').offset().top
            }, 0);
        };
    });

    $('#quiz-time').on('change', function () {
        $('#quiz-time-range').val($(this).val());
    });

    $('#quiz-time-range').on('change', function () {
        $('#quiz-time').val($(this).val());
    });

    $('#add-question-form').on('submit', function (e) {
        e.preventDefault();

        var formParams = $(this).serializeArray()[0];

        $.post('/admin/quizes/' + quiz.id + '/questions', { questionName: formParams.value })
            .done(function (res) {
                window.location.href = '/admin/quizes/' + quiz.id + '/questions/' + res.questionId;
            })
            .fail(function (error) {
                console.log(error);
            });
    });

    $('#update-quiz').on('click', function (e) {
        e.preventDefault();

        var quizData = {
            name: $('#quiz-form #quiz-name').val(),
            note: $('#quiz-form #additional-note').val(),
            time: parseInt($('#quiz-form #quiz-time').val()) * 60,
            random: $('#quiz-form #quiz-random input[type="checkbox"]').prop('checked'),
            topicId: parseInt($('#topics-form ul#topics-list>li>input[type="radio"]:checked').data('topic-id'))
        };

        $.post('/admin/quizes/' + quiz.id, { quizData: JSON.stringify(quizData) })
            .done(function (res) {
                showToast('Datele testului au fost actualizate cu succes', 1500);
            })
            .fail(function (error) {
                console.log(error);
            });
    });

    $('#remove-quiz').on('click', function () {
        confirmAction('Doriți să stergeți testul?', function (confirmResult) {
            if (confirmResult) {
                $.ajax({
                    url: '/admin/quizes/' + quiz.id,
                    type: 'DELETE',
                }).done(function (res) {
                    console.log(res);
                    window.location.href = '/admin/quizes';
                }).fail(function (error) {
                    console.log(error);
                });
            };
        });
    });

    $('#topics-search').on('keyup', _.debounce(function () {
        liveSearch();
    }, 100));

    function liveSearch() {
        var $input, filter, $ul, $li, $label;
        $input = $('#topics-search');
        filter = $input.val().toUpperCase();
        $ul = $('#topics-list');
        $li = $($ul).find('li');

        for (var i = 0; i < $li.length; i++) {
            $label = $($li[i]).find('label')[0];
            if ($($label).text().toUpperCase().indexOf(filter) > -1) {
                $($li[i]).css('display', '');
            } else {
                $($li[i]).css('display', 'none');
            };
        };

        $($input).parent().find('i.search-clear').on('click', function () {
            $($input).val('');
            $($input).keyup();
        });
    };

    function loadSidenav() {
        var template = Handlebars.templates['sidenav.tmpl'];
        $('#sidenav-wrapper').html(template({ pageTitle: 'Editarea testului' }));
    };

    function confirmAction(confirmMessage, callback, autoclose) {
        $('#quiz-confirm.modal .quiz-confirm-message').text(confirmMessage);
        $('#quiz-confirm.modal').modal();

        autoclose = autoclose || false;

        $('#quiz-confirm-no').on('click', function (e) {
            e.preventDefault();
            $('#quiz-confirm.modal').modal('close');
            callback(false);
        });

        $('#quiz-confirm-yes').off('click').on('click', function (e) {
            e.preventDefault();
            if (autoclose) {
                $('#quiz-confirm.modal').modal('close');
            };
            callback(true);
        });
    };

    function showToast(message, duration) {
        if (!$('.toast').length) {
            Materialize.toast(message, duration);
        };
    };
});