$(document).ready(function () {
    'use strict';

    question = JSON.parse(question);
    types = JSON.parse(types);
    console.log(question, types);

    loadSidenav();
    $('.collapsible').collapsible();
    $('.button-collapse').sideNav();
    $('select').material_select();

    $('#answers-form').on('change', 'input[type="checkbox"]', function () {
        if ($('select#question-type option:checked').text() == 'radio') {
            $('#answers-form input[type="checkbox"]').not(this).prop('checked', false);
        };
    });

    $('select#question-type').on('change', function () {
        if ($(this).find('option:checked').text() == 'radio' && $('#answers-form input[type="checkbox"]:checked').length > 1) {
            $('#answers-form input[type="checkbox"]').prop('checked', false);
        };
    });

    $('#add-answer').on('click', function () {
        var index = parseInt($('#answers-form .row:last .input-label').data('index')) || 0;
        index++;

        var $inputRow = `
             <div class="row">
                <div class="input-field col s11">
                    <input type="text" id="answer-${index}" class="answer-name">
                    <label for="answer-${index}" data-index="${index}" class="input-label">Răspuns ${index}</label>
                </div>
                <div class="input-field right">
                    <input name="answers" type="checkbox" id="correct-${index}">
                    <label for="correct-${index}"></label>
                </div>
            </div>`;

        $('#answers-form').append($inputRow);
        $('#answers-form .row:last').find('input.answer-name').focus();
    });

    $('#remove-answer').on('click', function () {
        $('#answers-form .row:last').remove();
        $('#answers-form .row:last').find('input.answer-name').focus();
    });

    $('#remove-question').on('click', function () {
        confirmAction('Doriti sa stergeti intrebarea?', function (confirmResult) {
            if (confirmResult) {
                $.ajax({
                    url: '/admin/quizes/' + question.quizId + '/questions/' + question.id,
                    type: 'DELETE',
                }).done(function (res) {
                    console.log(res);
                    window.location.href = '../#questions-tab';
                }).fail(function (error) {
                    console.log(error);
                });
            };
        });
    });

    $('#update-question').on('click', function () {
        var questionData = {
            name: $('input#question-name').val(),
            typeId: parseInt($('select#question-type').val()),
            points: parseInt($('input#question-points').val()),
            answers: []
        };

        $('#answers-form input[type="text"]').each(function (i, answerInput) {
            var isCorrect = $(answerInput).closest('.row').find('input[type="checkbox"]').prop('checked');
            questionData.answers.push({ name: $(answerInput).val(), isCorrect: isCorrect });
        });

        var url = '/admin/quizes/' + question.quizId + '/questions/' + question.id;
        $.post(url, { questionData: JSON.stringify(questionData) })
            .then(function (res, statusText, xhr) {
                console.log(res);
                showToast('Datele testului au fost actualizate cu succes', 1500);
            })
            .fail(function (error) {
                console.log(error);
                showToast('A aparut o eroare la actualizarea datelor intrebarii', 1500);
            });
    });

    function loadSidenav() {
        var template = Handlebars.templates['sidenav.tmpl'];
        $('#sidenav-wrapper').html(template({ pageTitle: 'Editarea întrebării' }));
    };

    function confirmAction(confirmMessage, callback, autoclose) {
        $('#quiz-confirm.modal .quiz-confirm-message').text(confirmMessage);
        $('#quiz-confirm.modal').modal();

        autoclose = autoclose || false;

        $('#quiz-confirm-no').on('click', function (e) {
            e.preventDefault();
            $('#quiz-confirm.modal').modal('close');
            callback(false);
        });

        $('#quiz-confirm-yes').off('click').on('click', function (e) {
            e.preventDefault();
            if (autoclose) {
                $('#quiz-confirm.modal').modal('close');
            };
            callback(true);
        });
    };

    function showToast(message, duration) {
        if (!$('.toast').length) {
            Materialize.toast(message, duration);
        };
    };
});