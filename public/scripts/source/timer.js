var timer = {
    counter: undefined,
    time: 0, //total seconds left for the countdown
    warning: 20, //when seconds==warning show message
    danger: 5,
    alert: function () {
        //whatever your warning message should do
        console.log("Hurry up!");
    },
    init: function (time) {
        this.time = time;
    },
    count: function () {
        this.time--;
        var minutes = Math.floor(this.time / 60);
        var seconds = this.time - minutes * 60;
        //tic tac
        if (this.time <= this.warning) {
            // warning
            $(".quiz-timer").toggleClass("red-text");
        };
        if (this.time <= this.danger) {
            // close to end
            $(".quiz-timer").removeClass("orange-text").toggleClass("red-text");
        };
        if (this.time == 0) {
            clearInterval(this.counter);
            this.finished();
        }
        // $(".quiz-timer").html(minutes + " : " + (seconds < 10 ? "0" + seconds : seconds));
    },
    getTime: function () {
        return this.time;
    },
    getSeconds() {
        return this.time - (Math.floor(this.time / 60) * 60);
    },
    getMinutes() {
        return (Math.floor(this.time / 60) * 60);
    },
    getFormatedTime() {
        var minutes = Math.floor(this.time / 60);
        var seconds = this.time - minutes * 60;
        return (minutes + ":" + (seconds < 10 ? "0" + seconds : seconds));
    },
    finished: function () {
        alert("Timpul a luat sfarsit!");
        // finish quiz
        Quiz.currentIndex = Quiz.questions.length;
        Quiz.init(Quiz.config);
    },
    stop: function () {
        clearInterval(this.counter);
        this.time = 0;
        $(".quiz-timer").html("0:00");
        $(".quiz-timer").removeClass("orange-text red-text").addClass("gray-text text-darken-4");
    }
};