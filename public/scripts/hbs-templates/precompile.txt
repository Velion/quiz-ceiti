To precompile a template, open cmd and write:

-------------------------------------------------------------------
$ handlebars <input_file_name> -f <output_file_name>.js
-------------------------------------------------------------------