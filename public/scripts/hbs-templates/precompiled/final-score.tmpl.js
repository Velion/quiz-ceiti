(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['final-score.tmpl'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                        <span class=\"light-green-text activator\">"
    + container.escapeExpression(((helper = (helper = helpers.mark || (depth0 != null ? depth0.mark : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"mark","hash":{},"data":data}) : helper)))
    + "</span> ";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " "
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.mark : depth0),">=",8,{"name":"ifCond","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return "\r\n                        <span class=\"lime-text activator\">"
    + container.escapeExpression(((helper = (helper = helpers.mark || (depth0 != null ? depth0.mark : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"mark","hash":{},"data":data}) : helper)))
    + "</span> ";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " "
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.mark : depth0),">=",6,{"name":"ifCond","hash":{},"fn":container.program(7, data, 0),"inverse":container.program(9, data, 0),"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    var helper;

  return "\r\n                        <span class=\"grey-text text-darken-2\">"
    + container.escapeExpression(((helper = (helper = helpers.mark || (depth0 != null ? depth0.mark : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"mark","hash":{},"data":data}) : helper)))
    + "</span> ";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " "
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.mark : depth0),"==",5,{"name":"ifCond","hash":{},"fn":container.program(10, data, 0),"inverse":container.program(12, data, 0),"data":data})) != null ? stack1 : "");
},"10":function(container,depth0,helpers,partials,data) {
    var helper;

  return "\r\n                        <span class=\"purple-text activator\">"
    + container.escapeExpression(((helper = (helper = helpers.mark || (depth0 != null ? depth0.mark : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"mark","hash":{},"data":data}) : helper)))
    + "</span> ";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " \r\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.mark : depth0),"<",5,{"name":"ifCond","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"13":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                                        <span class=\"red-text activator\">"
    + container.escapeExpression(((helper = (helper = helpers.mark || (depth0 != null ? depth0.mark : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"mark","hash":{},"data":data}) : helper)))
    + "</span>\r\n";
},"15":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {};

  return "<li class=\"collection-item quiz-answer-item\">\r\n                    <div>\r\n                        <span>"
    + container.escapeExpression(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</span>\r\n                    </div>\r\n                    <div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isCorrect : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.program(18, data, 0),"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.givenAnswers : depth0),{"name":"if","hash":{},"fn":container.program(20, data, 0),"inverse":container.program(23, data, 0),"data":data})) != null ? stack1 : "")
    + "                    </div>\r\n                </li>";
},"16":function(container,depth0,helpers,partials,data) {
    return "                        <i class=\"material-icons light-green-text left\">done_all</i>\r\n";
},"18":function(container,depth0,helpers,partials,data) {
    return "                        <i class=\"material-icons red-text left\">remove_circle</i>\r\n";
},"20":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.givenAnswers : depth0),{"name":"each","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"21":function(container,depth0,helpers,partials,data) {
    var alias1=container.escapeExpression;

  return "                                <span>"
    + alias1((helpers.counter || (depth0 && depth0.counter) || helpers.helperMissing).call(depth0 != null ? depth0 : {},(data && data.index),{"name":"counter","hash":{},"data":data}))
    + ")</span>    \r\n                                <span>"
    + alias1(container.lambda(depth0, depth0))
    + ",</span>\r\n";
},"23":function(container,depth0,helpers,partials,data) {
    return "                            <span>┐(￣～￣)┌</span>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<nav class=\"nav-extended white pink-text text-accent-4 lighten-1 nav-margin-bottom\">\r\n    <div class=\"nav-wrapper valign-wrapper\" style=\"vertical-align:top\">\r\n        <span class=\"nav-title\">Rezultate test</span>\r\n    </div>\r\n</nav>\r\n<div class=\"container container-small\">\r\n    <div class=\"row\">\r\n        <div class=\"col s12 m6 sl3\">\r\n            <div class=\"card horizontal quiz-result-card\">\r\n                <div class=\"card-image card-content valign-wrapper\">\r\n                    <i class=\"medium material-icons indigo-text\">assignment</i>\r\n                </div>\r\n                <div class=\"card-stacked\">\r\n                    <div class=\"card-content\">\r\n                        <div class=\"right-align\">\r\n                            <div class=\"grey-text text-darken-2\">\r\n                                <span class=\"quiz-result-stats\">"
    + alias4(((helper = (helper = helpers.totalScore || (depth0 != null ? depth0.totalScore : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"totalScore","hash":{},"data":data}) : helper)))
    + "</span>\r\n                            </div>\r\n                            <div class=\"blue-grey-text text-lighten-2\">Punctaj</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col s12 m6 sl3\">\r\n            <div class=\"card horizontal quiz-result-card\">\r\n                <div class=\"card-image card-content valign-wrapper\">\r\n                    <i class=\"medium material-icons light-green-text\">check_circle</i>\r\n                </div>\r\n                <div class=\"card-stacked\">\r\n                    <div class=\"card-content\">\r\n                        <div class=\"right-align\">\r\n                            <div class=\"grey-text text-darken-2\">\r\n                                <span class=\"quiz-result-stats\">"
    + alias4(((helper = (helper = helpers.correct || (depth0 != null ? depth0.correct : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"correct","hash":{},"data":data}) : helper)))
    + "</span>\r\n                            </div>\r\n                            <div class=\"blue-grey-text text-lighten-2\">Corecte</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col s12 m6 sl3\">\r\n            <div class=\"card horizontal quiz-result-card\">\r\n                <div class=\"card-image card-content valign-wrapper\">\r\n                    <i class=\"medium material-icons red-text\">remove_circle</i>\r\n                </div>\r\n                <div class=\"card-stacked\">\r\n                    <div class=\"card-content\">\r\n                        <div class=\"right-align\">\r\n                            <div class=\"grey-text text-darken-2\">\r\n                                <span class=\"quiz-result-stats\">"
    + alias4(((helper = (helper = helpers.incorrect || (depth0 != null ? depth0.incorrect : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"incorrect","hash":{},"data":data}) : helper)))
    + "</span>\r\n                            </div>\r\n                            <div class=\"blue-grey-text text-lighten-2\">Incorecte</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col s12 m6 sl3\">\r\n            <div class=\"card horizontal quiz-result-card\">\r\n                <div class=\"card-image card-content valign-wrapper\">\r\n                    <i class=\"medium material-icons purple-text\">report</i>\r\n                </div>\r\n                <div class=\"card-stacked\">\r\n                    <div class=\"card-content\">\r\n                        <div class=\"right-align\">\r\n                            <div class=\"grey-text text-darken-2\">\r\n                                <span class=\"quiz-result-stats\">"
    + alias4(((helper = (helper = helpers.skipped || (depth0 != null ? depth0.skipped : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"skipped","hash":{},"data":data}) : helper)))
    + "</span>\r\n                            </div>\r\n                            <div class=\"blue-grey-text text-lighten-2\">Fără răspuns</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col s12 m6 l4\">\r\n            <div class=\"card\">\r\n                <div class=\"waves-effect waves-block waves-dark\">\r\n                    <div class=\"quiz-mark center-align activator\" style=\"padding: 20px; font-size: 3.3rem;\">\r\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.mark : depth0),"==",10,{"name":"ifCond","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "                    </div>\r\n                </div>\r\n                <div class=\"card-content\">\r\n                    <span class=\"card-title\">Nota obținută</span>\r\n                </div>\r\n                <div class=\"card-reveal\">\r\n                    <span class=\"card-title\">\r\n                        Nota obținută\r\n                        <i class=\"material-icons right\">close</i>\r\n                    </span>\r\n                    <p>Stabilirea notei se face conform formulei ... Ipsum esse exercitation cillum Lorem nisi.</p>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"card\">\r\n                <div class=\"card-content\">\r\n                    <button type=\"button\" class=\"btn btn-flat btn-full waves-effect waves-dark blue-grey-text lighten-1\" id=\"quiz-startover\">\r\n                        <i class=\"material-icons left\">replay</i>\r\n                        Repornește testul\r\n                    </button>\r\n                    <a href=\"../\" class=\"btn btn-flat btn-full waves-effect waves-dark pink-text text-accent-4 lighten-1\" id=\"quiz-choose-topic\">                  \r\n                         <i class=\"material-icons left\">undo</i>\r\n                         Alege o altă temă\r\n                    </a>\r\n                    <span class=\"card-title\">Opțiuni</span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col s12 m6 l8\">\r\n            <div class=\"card\">\r\n                <div class=\"card-content\">\r\n                    <div style=\"width: 250px; height: 250px; margin: 0 auto;\" class=\"noselect\">\r\n                        <canvas id=\"chart\" width=\"250\" height=\"250\"></canvas>\r\n                    </div>\r\n                </div>\r\n                <div class=\"card-content\">\r\n                    <span class=\"card-title\">Statistica răspunsurilor</span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col s12\">\r\n            <ul class=\"collection with-header z-depth-1\">\r\n                <li class=\"collection-header\">\r\n                    <h5>Răspunsurile date</h5>\r\n                </li>"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.verifiedAnswers : depth0),{"name":"each","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "<li class=\"collection-item grey lighten-4\">\r\n                    <span>Întrebările fără răspuns dat sunt notate cu:</span><span class=\"grey-text text-lighten-1\">┐(￣～￣)┌</span>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</div>";
},"useData":true});
})();