(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['choices.tmpl'] = template({"1":function(container,depth0,helpers,partials,data,blockParams) {
    var stack1;

  return "<ul class=\"collection collection-borderless\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 1, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams})) != null ? stack1 : "")
    + "</ul>\r\n";
},"2":function(container,depth0,helpers,partials,data,blockParams) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "    <li class=\"collection-item collection-item-borderless collection-item-large quiz-choice-item\">\r\n        <input class=\"with-gap input-large option-input\" name=\"group3\" type=\"radio\" id=\"choice-"
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(alias5(blockParams[0][0], depth0))
    + "\" />\r\n        <label for=\"choice-"
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data,"blockParams":blockParams}) : helper)))
    + "\" class=\"capitalize\">"
    + alias4(alias5(blockParams[0][0], depth0))
    + "</label>\r\n    </li>\r\n";
},"4":function(container,depth0,helpers,partials,data,blockParams) {
    var stack1;

  return "<ul class=\"collection collection-borderless\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(5, data, 1, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams})) != null ? stack1 : "")
    + "</ul>\r\n";
},"5":function(container,depth0,helpers,partials,data,blockParams) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "    <li class=\"collection-item collection-item-borderless collection-item-large quiz-choice-item\">\r\n        <input type=\"checkbox\" class=\"input-large option-input\" id=\"choice-"
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(alias5(blockParams[0][0], depth0))
    + "\" />\r\n        <label for=\"choice-"
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data,"blockParams":blockParams}) : helper)))
    + "\" class=\"capitalize\">"
    + alias4(alias5(blockParams[0][0], depth0))
    + "</label>\r\n    </li>\r\n";
},"7":function(container,depth0,helpers,partials,data,blockParams) {
    var stack1;

  return "<ul class=\"collection collection-borderless\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(8, data, 1, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams})) != null ? stack1 : "")
    + "</ul>\r\n";
},"8":function(container,depth0,helpers,partials,data,blockParams) {
    var helper, alias1=container.escapeExpression;

  return "    <li class=\"collection-item collection-item-borderless collection-item-large quiz-choice-item\">\r\n        <span class=\"capitalize\" id=\"choice-"
    + alias1(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"index","hash":{},"data":data}) : helper)))
    + "\">"
    + alias1(container.lambda(blockParams[0][0], depth0))
    + "</span>\r\n        <div class=\"switch\">\r\n            <label>\r\n                Fals\r\n                <input type=\"checkbox\">\r\n                <span class=\"lever\"></span>\r\n                Adevarat\r\n            </label>\r\n        </div>\r\n    </li>\r\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "    <div class=\"col s12 m4 offset-m4\">\r\n        <div class=\"input-field\">\r\n            <i class=\"material-icons prefix\">create</i>\r\n            <input type=\"text\" class=\"form-control validate\" id=\"quiz-text-input\" autofocus>\r\n            <label for=\"quiz-text-input\">Raspuns</label>\r\n        </div>\r\n    </div>\r\n</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams) {
    var stack1, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"==","radio",{"name":"ifCond","hash":{},"fn":container.program(1, data, 0, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"==","checkbox",{"name":"ifCond","hash":{},"fn":container.program(4, data, 0, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"==","truefalse",{"name":"ifCond","hash":{},"fn":container.program(7, data, 0, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"==","write",{"name":"ifCond","hash":{},"fn":container.program(10, data, 0, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams})) != null ? stack1 : "");
},"useData":true,"useBlockParams":true});
})();