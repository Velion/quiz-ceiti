var express = require('express');
var router = express.Router();
var Q = require('q');
var moment = require('moment');
var connection = require('../configs/connection');
var services = require('../configs/services');

// setting moment.js locale to romanian
moment.locale('ro');

router.get('/:topicId', function (req, res, next) {
    var dataToSend = {}
    var quizList = [];
    var quizTopicId = parseInt(req.params.topicId) || 0;

    services.quizes.queryQuizes(null, quizTopicId)
        .then(function (quizes) {
            quizes.forEach(function (quiz) {
                quizList.push({
                    id: parseInt(quiz.id),
                    name: quiz.name,
                    topicId: parseInt(quiz.topicId),
                    topicName: quiz.topicName,
                    knowledgeOf: quiz.knowledgeOf,
                    randomShuffle: quiz.shuffle,
                    timeLimit: Math.floor(quiz.time / 60),
                    timeSeconds: quiz.time,
                    createdOn: {
                        fromNow: moment(quiz.createdOn).fromNow(),
                        date: moment(quiz.createdOn).format('ll')
                    }
                });
            });

            dataToSend.quizList = quizList;
            res.render('quiz-list', dataToSend);
        }).fail(function (error) {
            res.status(400).end("Invalid quiz Id");
        });
});

module.exports = router;