var express = require('express');
var router = express.Router();
var Q = require('q');
var bcrypt = require('bcryptjs');
var connection = require('../configs/connection');
var services = require('../configs/services');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var visitCounter = 0;

function createNewError(status, message) {
	var error = new Error(message);
	error.status = status;
	error.statusMessage = message;

	return error;
};


// router.use(cookieSession({
// 	name: 'session2',
// 	keys: ['key1']
// }));


router.get('/', function (req, res, next) {
	console.log("1. " + visitCounter);
	console.log("ReqCounter " + req.session.views);
	req.session.views = (req.session.views || 0) + 1;
	visitCounter = req.session.views;
	console.log("2. " + visitCounter);
	// get topic data sent from quiz.js via params
	if (req.query.topicData && req.query.quizId) {
		topicData = req.query.topicData;
		quizId = parseInt(req.query.quizId) || 1;
		quizPassword = req.query.quizPassword.toString();
		quizUser = req.query.quizUser.toString();
		quizTime = parseInt(req.query.quizTime);
	};

	services.quizes.queryQuizPassword(quizId)
		.then(function (hash) {
			return services.quizes.verifyQuizPassword(quizPassword, hash);
		})
		.then(function (isCorrectPassword) {
			services.quizes.queryQuizQuestions(quizId)
				.then(function (questions) {
					// database GROUP_CONCAT selected separator
					var separator = '~~';
					var questionsData = [];
					questions.forEach(function (question) {
						console.log(question);
						questionsData.push({
							id: parseInt(question.id),
							name: question.name,
							type: question.type,
							points: parseInt(question.points) || 0,
							answers: (question.answers) ? question.answers.split(separator) : [],
							correctAnswers: (question.correctAnswers) ? question.correctAnswers.split(separator) : []
						});
					});
					// data to send back to user
					let dataToSend = {
						quizData: {
							topicData: topicData,
							questions: questionsData,
							quizUser: quizUser,
							quizTime: quizTime,
							quizId: quizId
						}
					};

					return dataToSend;
				})
				.then(function (dataToSend) {
					// var randomNumber = Math.random().toString();//ca sa fie 
					// randomNumber = randomNumber.substring(2, randomNumber.length);//ca sa fie 
					// randomNumber = 8;
					// res.cookie('cookieName', randomNumber, { maxAge: 900000 });
					// console.log('cookie created successfully');
					console.log(visitCounter + " counter 3");
					// res.render('quiz', dataToSend);
					if ((visitCounter - 1) > 1) {
						res.redirect('/');
						req.session.destroy();
					} else {
						res.render('quiz', dataToSend);
						visitCounter = 0;
					}
					// ((visitCounter - 1) > 1) ? res.render('quiz-list') : res.render('quiz', dataToSend);
					console.log(dataToSend);
				})
				.fail(function (error) {
					res.send(createNewError(500, 'Quiz data cound not be found'));
				});
		})
		.fail(function (error) {
			res.send(createNewError(400, 'Passwords do not match!'));
		});
});


module.exports = router;
