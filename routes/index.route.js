var express = require('express');
var router = express.Router();
var connection = require('../configs/connection');
var services = require('../configs/services');

router.get('/', function (req, res, next) {
	var dataToSend = { topics: [] };
	var topics = [];

	services.topics.queryTopicsList().then(function (topics) {
		topics.forEach(function (topic) {
			topic['icon'] = (topic.id == 1) ? null :
				(topic.id < 10) ? 'devicon-html5-plain' :
					(topic.id < 14) ? 'devicon-css3-plain' :
						(topic.id < 17) ? 'devicon-php-plain' : 'devicon-javascript-plain';
		});

		return topics;
	}).then(function (topics) {
		dataToSend.topics = topics;

		res.render('index', dataToSend);
	});
});

module.exports = router;
